export class Campaign {
    campaignName: string;
    amount: number;
    value: number;
    paymentAddress: string;
}
