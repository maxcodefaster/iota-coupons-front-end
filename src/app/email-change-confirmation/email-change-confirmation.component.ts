import { Component, OnInit, NgZone, AfterViewInit, ViewChild } from '@angular/core';
import { ParamMap, ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../shared/user.service';
import { NgForm, FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

// import custom validator to validate that email and confirm email fields match
import { MustMatch } from '../_helpers/must-match.validator';
import { RecaptchaComponent } from 'ng-recaptcha';

@Component({
  selector: 'app-email-change-confirmation',
  templateUrl: './email-change-confirmation.component.html',
  styleUrls: ['./email-change-confirmation.component.css']
})
export class EmailChangeConfirmationComponent implements OnInit {
  @ViewChild('captchaRef', { static: true }) captchaRef: RecaptchaComponent;

  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  serverMessage: string;
  responseType;
  captchaResponse;
  submitted = false;
  loggedIn = false;
  changeEmailForm: FormGroup;
  token;
  model = {
    email: '',
    password: '',
    recaptcha: '',
  };

  ngOnInit() {
    if (this.userService.isLoggedIn()) {
      this.loggedIn = true;
    }
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.token = params.get('id');
    });
  }



  constructor(private fb: FormBuilder, private route: ActivatedRoute, private userService: UserService, private router: Router,
    private _snackBar: MatSnackBar, private zone: NgZone) {
    // To initialize FormGroup
    this.changeEmailForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      confirmEmail: ['', [Validators.required]],
      recaptchaReactive: [null, Validators.required]
    }, {
      validator: MustMatch('email', 'confirmEmail')
    });
  }

  changeEmail() {
    this.captchaRef.resolved.subscribe(captchaResponse => this.changeEmailSubmit(captchaResponse, this.changeEmailForm));
    this.captchaRef.execute();
  }

  changeEmailSubmit(captchaResponse, form) {
    // stop here if form is invalid
    if (this.changeEmailForm.invalid) {
      return;
    }
    form.value.recaptcha = captchaResponse;
    form.value.token = this.token;
    this.userService.updateEmail(form.value).subscribe(
      (res: any) => {
        console.log(res);
        this.serverMessage = res.message;
        this.responseType = res.type;
        this.openSnackBar(this.serverMessage);
        this.submitted = true;
        if (this.loggedIn) {
          this.userService.deleteToken();
        }
        this.captchaRef.reset();
      },
      err => {
        console.log(err);
        this.responseType = err.error.type;
        this.serverMessage = err.error.message;
        this.openSnackBar(this.serverMessage);
        this.captchaRef.reset();
      }
    );
  }

  // convenience getter for easy access to form fields
  get f() { return this.changeEmailForm.controls; }

  onSubmitLogin(form: NgForm) {
    this.userService.login(form.value).subscribe(
      res => {
        this.userService.setToken(res['token']);
        this.router.navigateByUrl('/dashboard');
      },
      err => {
        this.serverMessage = err.error.message;
        this.openSnackBar(this.serverMessage);
      }
    );
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'OK', {
      duration: 2000,
    });
  }

}
