import { Component, OnInit, NgZone, ViewChild, AfterViewInit } from '@angular/core';
import { Router } from '@angular/router';

import { UserService } from '../shared/user.service';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MatDialogRef, MatDialog } from '@angular/material/dialog';
import { FormControl, FormGroupDirective, NgForm, Validators, FormBuilder, FormGroup } from '@angular/forms';

// import custom validator to validate that password and confirm password fields match
import { MustMatch } from '../_helpers/must-match.validator';
import { RecaptchaComponent } from 'ng-recaptcha';

@Component({
  selector: 'app-user',
  templateUrl: './user.component.html',
  styleUrls: ['./user.component.css']
})
export class UserComponent implements OnInit {

  loginForm: FormGroup;

  constructor(private userService: UserService, private fb: FormBuilder, private router: Router, private _snackBar: MatSnackBar, public dialog: MatDialog, ) {
    // To initialize FormGroup
    this.loginForm = fb.group({
      'email': [null],
      'password': [null],
    });
  }

  model = {
    email: '',
    password: '',
  };

  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  showSucessMessage: boolean;
  serverErrorMessages: string;

  ngOnInit() {
    if (this.userService.isLoggedIn()) {
      this.router.navigateByUrl('/dashboard');
    }
  }


  onSubmitLogin(form: NgForm) {
    this.userService.login(form.value).subscribe(
      res => {
        this.userService.setToken(res['token']);
        this.router.navigateByUrl('/dashboard');
      },
      err => {
        this.serverErrorMessages = err.error.message;
        this.openSnackBar(this.serverErrorMessages);
      }
    );
  }

  openRegisterDialog(): void {
    // tslint:disable-next-line: no-use-before-declare
    const dialogRef = this.dialog.open(RegisterDialogComponent, {
      autoFocus: true,
      width: '340px',
    });

    dialogRef.afterClosed().subscribe(() => {
    });
  }

  openPwForgetDialog(): void {
    // tslint:disable-next-line: no-use-before-declare
    const dialogRef = this.dialog.open(PwForgetDialogComponent, {
      autoFocus: true,
      width: '340px',
    });

    dialogRef.afterClosed().subscribe(() => {
    });
  }

  focusEmailInput() {
    document.getElementById('email').focus();
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'OK', {
      duration: 2000,
    });
  }


}

@Component({
  selector: 'app-register-dialog',
  templateUrl: 'app-register-dialog.html',
})
export class RegisterDialogComponent implements OnInit {
  @ViewChild('captchaRef', { static: true }) captchaRef: RecaptchaComponent;
  signUpForm: FormGroup;
  serverMessage: string;
  submitted = false;

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<RegisterDialogComponent>, public userService: UserService,
    private _snackBar: MatSnackBar, private router: Router, private zone: NgZone) { }


  ngOnInit() {
    this.signUpForm = this.fb.group({
      fullName: ['', Validators.required],
      email: ['', [Validators.required, Validators.email]],
      password: ['', [Validators.required, Validators.minLength(8)]],
      confirmPassword: ['', Validators.required],
      acceptTerms: [false, Validators.requiredTrue],
      recaptchaReactive: [null, Validators.required]
    }, {
      validator: MustMatch('password', 'confirmPassword')
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.signUpForm.controls; }

  onSubmit() {
    this.captchaRef.resolved.subscribe(captchaResponse => this.submit(captchaResponse, this.signUpForm));
    this.captchaRef.execute();
  }

  submit(captchaResponse: string, form) {
    this.submitted = true;
    // stop here if form is invalid
    if (this.signUpForm.invalid) {
      return;
    }
    form.value.recaptcha = captchaResponse;
    this.userService.postUser(form.value).subscribe(
      (res: any) => {
        this.serverMessage = res.message;
        setTimeout(() => this.dialogRef.close(), 8000);
        this.captchaRef.reset();
      },
      err => {
        if (err.status === 422) {
          this.serverMessage = err.error.message;
          this.openSnackBar(this.serverMessage);
        } else {
          this.serverMessage = 'Something went wrong. Please contact admin.';
          this.openSnackBar(this.serverMessage);
        }
        this.captchaRef.reset();
      }
    );

  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'OK', {
      duration: 2000,
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}

@Component({
  selector: 'app-pw-forget-dialog',
  templateUrl: 'app-pw-forget-dialog.html',
})
export class PwForgetDialogComponent implements OnInit {
  @ViewChild('captchaRef', { static: true }) captchaRef: RecaptchaComponent;
  pwForgetForm: FormGroup;
  serverMessage: string;
  submitted = false;


  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<RegisterDialogComponent>, public userService: UserService,
    private _snackBar: MatSnackBar, private router: Router, private zone: NgZone) { }


  ngOnInit() {
    this.pwForgetForm = this.fb.group({
      email: ['', [Validators.required, Validators.email]],
      recaptchaReactive: [null, Validators.required]
    });
  }

  // convenience getter for easy access to form fields
  get f() { return this.pwForgetForm.controls; }

  onSubmit() {
    this.captchaRef.resolved.subscribe(captchaResponse => this.submit(captchaResponse, this.pwForgetForm));
    this.captchaRef.execute();
  }

  submit(captchaResponse: string, form) {
    this.submitted = true;
    // stop here if form is invalid
    if (this.pwForgetForm.invalid) {
      return;
    }
    form.value.recaptcha = captchaResponse;
    this.userService.reqPwChange(form.value).subscribe(
      (res: any) => {
        this.serverMessage = res.message;
        setTimeout(() => this.dialogRef.close(), 8000);
        this.captchaRef.reset();
      },
      err => {
        this.serverMessage = err.error.message;
        this.openSnackBar(this.serverMessage);
        this.captchaRef.reset();
      }
    );
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'OK', {
      duration: 2000,
    });
  }

  onNoClick(): void {
    this.dialogRef.close();
  }

}
