// built-in
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { RouterModule } from '@angular/router';
import { HttpClientModule, HTTP_INTERCEPTORS } from '@angular/common/http';

// routes
import { appRoutes } from './routes';
import { UserService } from './shared/user.service';
// other
import { AuthGuard } from './auth/auth.guard';
import { AuthInterceptor } from './auth/auth.interceptor';

// Material Components
import { MatCardModule } from '@angular/material/card';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatIconModule } from '@angular/material/icon';
import { MatGridListModule } from '@angular/material/grid-list';
import { MatTabsModule } from '@angular/material/tabs';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatDialogModule } from '@angular/material/dialog';
import {MatTooltipModule} from '@angular/material/tooltip';
import {MatTableModule} from '@angular/material/table';
import { MatSortModule } from '@angular/material/sort';
import {MatChipsModule} from '@angular/material/chips';
import {MatSnackBarModule} from '@angular/material/snack-bar';
import {MatStepperModule} from '@angular/material/stepper';
import {MatPaginatorModule} from '@angular/material/paginator';
import {MatCheckboxModule} from '@angular/material/checkbox';
import {MatProgressSpinnerModule} from '@angular/material/progress-spinner';
import {MatExpansionModule} from '@angular/material/expansion';
import {MatDividerModule} from '@angular/material/divider';

// ngx-color-picker
import { ColorPickerModule } from 'ngx-color-picker';

// ngx-bootstrap components
import { CarouselModule } from 'ngx-bootstrap/carousel';

// Recaptcha
import { RecaptchaModule, RecaptchaFormsModule } from 'ng-recaptcha';

// QR
import { QRCodeModule } from 'angularx-qrcode';

// PDF Viewer
import { NgxExtendedPdfViewerModule } from 'ngx-extended-pdf-viewer';

// components
import { AppComponent } from './app.component';
import { RegisterDialogComponent, UserComponent, PwForgetDialogComponent } from './user/user.component';
import { AddDialogComponent, DashboardComponent } from './dashboard/dashboard.component';
import { HeaderComponent } from './header/header.component';
import { StartComponent } from './start/start.component';
import { CampaignComponent } from './campaign/campaign.component';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { FeatureSliderComponent } from './feature-slider/feature-slider.component';
import { RedeemComponent } from './redeem/redeem.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { SettingsComponent } from './settings/settings.component';
import { EmailChangeConfirmationComponent } from './email-change-confirmation/email-change-confirmation.component';
import { PwConfirmationComponent } from './pw-confirmation/pw-confirmation.component';
import { FooterComponent } from './footer/footer.component';
import { TosComponent } from './tos/tos.component';
import { ServiceWorkerModule } from '@angular/service-worker';
import { environment } from '../environments/environment';

@NgModule({
  declarations: [
    AppComponent,
    UserComponent,
    DashboardComponent,
    AddDialogComponent,
    RegisterDialogComponent,
    PwForgetDialogComponent,
    HeaderComponent,
    StartComponent,
    CampaignComponent,
    FeatureSliderComponent,
    RedeemComponent,
    ConfirmationComponent,
    SettingsComponent,
    EmailChangeConfirmationComponent,
    PwConfirmationComponent,
    FooterComponent,
    TosComponent,
  ],
  imports: [
    BrowserAnimationsModule,
    BrowserModule,
    FormsModule,
    ReactiveFormsModule,
    RouterModule.forRoot(appRoutes),
    HttpClientModule,
    MatToolbarModule,
    MatCardModule,
    MatGridListModule,
    MatIconModule,
    MatButtonModule,
    MatTabsModule,
    MatFormFieldModule,
    MatListModule,
    MatDialogModule,
    MatInputModule,
    MatTooltipModule,
    MatTableModule,
    MatSortModule,
    MatChipsModule,
    MatSnackBarModule,
    MatStepperModule,
    MatPaginatorModule,
    MatCheckboxModule,
    MatProgressSpinnerModule,
    MatExpansionModule,
    MatDividerModule,
    QRCodeModule,
    NgxExtendedPdfViewerModule,
    CarouselModule.forRoot(),
    RecaptchaModule,
    RecaptchaFormsModule,
    ColorPickerModule,
    ServiceWorkerModule.register('ngsw-worker.js', { enabled: environment.production }),
  ],
  entryComponents: [AddDialogComponent, RegisterDialogComponent, PwForgetDialogComponent],
  providers: [{
    provide: HTTP_INTERCEPTORS,
    useClass: AuthInterceptor,
    multi: true
  }, AuthGuard, UserService],
  bootstrap: [AppComponent]
})
export class AppModule { }
