import { Routes } from '@angular/router';
import { StartComponent } from './start/start.component';
import { UserComponent } from './user/user.component';
import { ConfirmationComponent } from './confirmation/confirmation.component';
import { PwConfirmationComponent } from './pw-confirmation/pw-confirmation.component';
import { EmailChangeConfirmationComponent } from './email-change-confirmation/email-change-confirmation.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { CampaignComponent } from './campaign/campaign.component';
import { SettingsComponent } from './settings/settings.component';
import { TosComponent } from './tos/tos.component';
import { AuthGuard } from './auth/auth.guard';

export const appRoutes: Routes = [
    {
        path: 'start', component: StartComponent
    },
    {
        path: 'tos', component: TosComponent
    },
    {
        path: 'dashboard', component: DashboardComponent, canActivate: [AuthGuard]
    },
    {
        path: 'settings', component: SettingsComponent, canActivate: [AuthGuard]
    },
    {
        path: 'campaign/:id', component: CampaignComponent, canActivate: [AuthGuard]
    },
    {
        path: 'confirmation/:id', component: ConfirmationComponent
    },
    {
        path: 'email-change-confirmation/:id', component: EmailChangeConfirmationComponent
    },
    {
        path: 'pw-confirmation/:id', component: PwConfirmationComponent
    },
    {
        path: '', redirectTo: '/start', pathMatch: 'full'
    },
    {
        path: '**', component: StartComponent
    }

];
