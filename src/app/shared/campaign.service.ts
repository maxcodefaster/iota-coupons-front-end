import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { Campaign } from './campaign.model';

@Injectable({
  providedIn: 'root'
})
export class CampaignService {
  selectedCampaign: Campaign = {
    campaignName: '',
    amount: 0,
    value: 0,
    paymentAddress: ''
  };

  noAuthHeader = { headers: new HttpHeaders({ 'NoAuth': 'True' }) };

  constructor(private http: HttpClient) { }

// HTTP Methods


postCampaign(campaign: Campaign) {
  return this.http.post(environment.apiBaseUrl + '/campaigns', campaign);
}

getCampaign() {
  return this.http.get(environment.apiBaseUrl + '/campaigns');
}

getSpecificCampaign(campaignId) {
  return this.http.get(environment.apiBaseUrl + '/campaigns/' + campaignId);
}

getCoupons(id) {
  return this.http.get(environment.apiBaseUrl + '/campaigns/' + id + '/coupons');
}

// PDF
getBorders() {
  return this.http.get(environment.apiBaseUrl + '/borders');
}

getLogos() {
  return this.http.get(environment.apiBaseUrl + '/logos');
}

createPdf(coupons) {
  return this.http.post(environment.apiBaseUrl + '/pdf', coupons);
}

}
