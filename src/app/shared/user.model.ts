export class User {
    fullName: string;
    email: string;
    password: string;
    recaptcha: string;
}
