import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-start',
  templateUrl: './start.component.html',
  styleUrls: ['./start.component.css']
})
export class StartComponent implements OnInit {

  constructor() { }

  ngOnInit() {
  }

  scrollToElement(target): void {
    const element = document.querySelector(target);
    element.scrollIntoView({ behavior: 'smooth', block: 'start', inline: 'nearest' });
  }

}
