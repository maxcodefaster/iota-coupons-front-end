import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class RedeemService {

  noAuthHeader = { headers: new HttpHeaders({ 'NoAuth': 'True' }) };

  constructor(private http: HttpClient) { }

  // HttpMethods

  redeemCoupon(coupon) {
    return this.http.post(environment.apiBaseUrl + '/payoutCoupon', coupon, this.noAuthHeader);
  }

}
