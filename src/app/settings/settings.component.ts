import { Component, OnInit, NgZone, ViewChild, AfterViewInit } from '@angular/core';
import { UserService } from '../shared/user.service';
import { FormBuilder, FormGroup, Validators, FormsModule, NgForm, FormControl } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';
import { RecaptchaComponent } from 'ng-recaptcha';


@Component({
  selector: 'app-settings',
  templateUrl: './settings.component.html',
  styleUrls: ['./settings.component.css']
})
export class SettingsComponent implements OnInit {
  @ViewChild('captchaRef', { static: true }) captchaRef: RecaptchaComponent;

  userDetails;
  changeNameForm: FormGroup;
  serverMessage;
  emailButtonColor = 'primary';
  pwButtonColor = 'primary';

  constructor(private fb: FormBuilder, private userService: UserService, private _snackBar: MatSnackBar, private zone: NgZone) {
    // To initialize FormGroup
    this.changeNameForm = fb.group({
      'fullName': new FormControl('', [
        Validators.required,
      ]),
    });
  }

  ngOnInit() {
    this.getUserProfile();
  }

  getUserProfile() {
    this.userService.getUserProfile().subscribe(
      res => {
        this.userDetails = res['user'];
      },
      err => {
        console.log(err);
      }
    );
  }

  changeName(form) {
    // stop here if form is invalid
    if (this.changeNameForm.invalid) {
      return;
    }
    form.value.email = this.userDetails.email;
    this.userService.changeName(form.value).subscribe(
      (res: any) => {
        this.serverMessage = res.message;
        this.getUserProfile();
        this.openSnackBar(this.serverMessage);
      },
      err => {
        if (err.status === 500) {
          this.serverMessage = err.error.message;
          this.openSnackBar(this.serverMessage);
        }
      }
    );
  }

  changeEmail() {
    this.userService.reqEmailChange().subscribe(
      (res: any) => {
        this.emailButtonColor = 'accent';
        this.serverMessage = res.message;
        this.openSnackBar(this.serverMessage);
      },
      err => {
        this.emailButtonColor = 'warn';
        this.serverMessage = err.error.message;
        this.openSnackBar(this.serverMessage);
      }
    );
  }

  changePw() {
    this.captchaRef.resolved.subscribe(captchaResponse => this.changePwSubmit(captchaResponse));
    this.captchaRef.execute();
  }

  changePwSubmit(captchaResponse: string) {
    const body = { recaptcha: captchaResponse, email: this.userDetails.email };
    this.userService.reqPwChange(body).subscribe(
      (res: any) => {
        this.emailButtonColor = 'accent';
        this.serverMessage = res.message;
        this.openSnackBar(this.serverMessage);
        this.captchaRef.reset();
      },
      err => {
        this.emailButtonColor = 'warn';
        this.serverMessage = err.error.message;
        this.openSnackBar(this.serverMessage);
        this.captchaRef.reset();
      }
    );
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'OK', {
      duration: 5000,
    });
  }

}
