import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { environment } from '../../environments/environment';
import { User } from './user.model';

@Injectable({
  providedIn: 'root'
})
export class UserService {
  selectedUser: User = {
    fullName: '',
    email: '',
    password: '',
    recaptcha: '',
  };

  noAuthHeader = { headers: new HttpHeaders({ 'NoAuth': 'True' }) };

  constructor(private http: HttpClient) { }

  // HttpMethods

  postUser(user) {
    return this.http.post(environment.apiBaseUrl + '/register', user, this.noAuthHeader);
  }

  login(authCredentials) {
    return this.http.post(environment.apiBaseUrl + '/authenticate', authCredentials, this.noAuthHeader);
  }

  getUserProfile() {
    return this.http.get(environment.apiBaseUrl + '/userProfile');
  }

  confirmUser(tokenId) {
    return this.http.get(environment.apiBaseUrl + '/confirmation/' + tokenId, this.noAuthHeader);
  }

  // Helper Methods

  setToken(token: string) {
    localStorage.setItem('token', token);
  }

  getToken() {
    return localStorage.getItem('token');
  }

  deleteToken() {
    localStorage.removeItem('token');
  }

  changeName(user) {
    return this.http.post(environment.apiBaseUrl + '/changeName', user);
  }

  reqEmailChange() {
    return this.http.get(environment.apiBaseUrl + '/reqEmailChange');
  }

  updateEmail(email) {
    return this.http.post(environment.apiBaseUrl + '/updateEmail', email);
  }

  reqPwChange(body) {
    return this.http.post(environment.apiBaseUrl + '/reqPwChange', body);
  }

  updatePw(body) {
    return this.http.post(environment.apiBaseUrl + '/updatePw', body);
  }

  getUserPayload() {
    const token = this.getToken();
    if (token) {
      const userPayload = atob(token.split('.')[1]);
      return JSON.parse(userPayload);
    } else {
      return null;
    }
  }

  isLoggedIn() {
    const userPayload = this.getUserPayload();
    if (userPayload) {
      return userPayload.exp > Date.now() / 1000;
    } else {
      return false;
    }
  }

}
