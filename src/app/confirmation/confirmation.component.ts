import { Component, OnInit } from '@angular/core';
import { ParamMap, ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../shared/user.service';
import { NgForm } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

@Component({
  selector: 'app-confirmation',
  templateUrl: './confirmation.component.html',
  styleUrls: ['./confirmation.component.css']
})
export class ConfirmationComponent implements OnInit {
  serverMessage;
  responseType;
  model = {
    email: '',
    password: '',
    recaptcha: '',
  };
  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;

  constructor(private route: ActivatedRoute, private userService: UserService, private router: Router, private _snackBar: MatSnackBar) { }

  ngOnInit() {
    if (this.userService.isLoggedIn()) {
      this.router.navigateByUrl('/dashboard');
    }
    let token = '';
    this.route.paramMap.subscribe((params: ParamMap) => {
      token = params.get('id');
    });
    this.confirmUser(token);
  }

  confirmUser(id) {
    this.userService.confirmUser(id).subscribe(
      (res: any) => {
        this.serverMessage = res.message;
        this.responseType = res.type;
        // this.router.navigate(['/start']);
      },
      err => {
        this.serverMessage = err.error.message;
        this.responseType = err.error.type;
        console.log(err);
      }
    );
  }

  onSubmitLogin(form: NgForm) {
    this.userService.login(form.value).subscribe(
      res => {
        this.userService.setToken(res['token']);
        this.router.navigateByUrl('/dashboard');
      },
      err => {
        this.serverMessage = err.error.message;
        this.openSnackBar(this.serverMessage);
      }
    );
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'OK', {
      duration: 2000,
    });
  }

}
