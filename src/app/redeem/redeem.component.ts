import { Component, OnInit, NgZone, ViewChild, AfterViewInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { RedeemService } from '../shared/redeem.service';
import { RecaptchaComponent } from 'ng-recaptcha';

@Component({
  selector: 'app-redeem',
  templateUrl: './redeem.component.html',
  styleUrls: ['./redeem.component.css']
})
export class RedeemComponent implements OnInit {
  @ViewChild('captchaRef', { static: true }) captchaRef: RecaptchaComponent;

  redeemForm: FormGroup;
  serverMessages: string;

  constructor(private fb: FormBuilder, private redeemService: RedeemService, private zone: NgZone) {
    // To initialize FormGroup
    this.redeemForm = this.fb.group({
      code: [null, Validators.required],
      address: [null, Validators.required],
      recaptchaReactive: [null, Validators.required]
    });
  }

  ngOnInit() {
  }

  onSubmit() {
    this.captchaRef.resolved.subscribe(captchaResponse => this.submit(captchaResponse, this.redeemForm));
    this.captchaRef.execute();
  }

  submit(captchaResponse, form) {
    if (!this.redeemForm.valid) {
      return;
    }
    this.captchaRef.execute();
    form.recaptcha = captchaResponse;
    this.redeemService.redeemCoupon(form).subscribe(
      (res: any) => {
        this.serverMessages = res.message;
        this.captchaRef.reset();
        this.captchaRef.execute();
      },
      err => {
        this.serverMessages = err.error.message;
        this.captchaRef.reset();
        this.captchaRef.execute();
      }
    );
  }

}
