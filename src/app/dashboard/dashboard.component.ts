
export class MatTableDataSourceWithCustomSort<T> extends MatTableDataSource<T> {

  _compareFn = new Intl.Collator('pl', { sensitivity: 'base', numeric: true }).compare;

  sortData: ((data: T[], sort: MatSort) => T[]) = (data: T[], sort: MatSort): T[] => {
    const active = sort.active;
    const direction = sort.direction;
    if (!active || direction === '') { return data; }

    if (active === 'total') {

      return data.sort((a, b) => {
        const aValue = this.sortingDataAccessor(a, 'value');
        const aAmount = this.sortingDataAccessor(a, 'amount');
        const bValue = this.sortingDataAccessor(b, 'value');
        const bAmount = this.sortingDataAccessor(b, 'amount');

        const aTotal = Number.parseFloat(aValue.toString()) * Number.parseFloat(aAmount.toString());
        const bTotal = Number.parseFloat(bValue.toString()) * Number.parseFloat(bAmount.toString());

        const comparatorResult = this._compareFn(<string>aTotal.toString(), <string>bTotal.toString());

        return comparatorResult * (direction === 'asc' ? 1 : -1);
      });
    } else {

      return data.sort((a, b) => {
        const valueA = this.sortingDataAccessor(a, active);
        const valueB = this.sortingDataAccessor(b, active);

        const comparatorResult = this._compareFn(<string>valueA, <string>valueB);

        return comparatorResult * (direction === 'asc' ? 1 : -1);
      });
    }
  }
}


import { Component, OnInit, ViewChild, AfterViewInit } from '@angular/core';
import { animate, state, style, transition, trigger } from '@angular/animations';
import { CampaignService } from '../shared/campaign.service';
import { FormBuilder, FormGroup, Validators, FormsModule, NgForm, FormControl } from '@angular/forms';
import { MatDialogRef, MAT_DIALOG_DATA, MatDialog } from '@angular/material/dialog';
import { Router } from '@angular/router';
import { MatSort } from '@angular/material/sort';
import { MatTableDataSource } from '@angular/material/table';
import { MatPaginator } from '@angular/material/paginator';
import { Campaign } from '../shared/campaign.model';
import { UserService } from '../shared/user.service';




@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css'],
})

export class DashboardComponent implements OnInit {
  campaigns;
  campaignsAmount = 0;
  totalCoupons = 0;
  couponsRedeemed = 0;
  totalMiota = 0;
  displayedColumns: string[] = ['campaignName', 'value', 'amount', 'total', 'redeemed', 'date', 'payed', 'action'];
  campaignsList;
  dataSource;
  userDetails;



  @ViewChild(MatSort, { static: true }) sort: MatSort;
  @ViewChild(MatPaginator, { static: true }) paginator: MatPaginator;

  constructor(private userService: UserService, private campaignService: CampaignService, public dialog: MatDialog,
    private router: Router) { }

  ngOnInit() {
    this.getUserProfile();
    this.getCampaigns();
  }

  getUserProfile() {
    this.userService.getUserProfile().subscribe(
      res => {
        this.userDetails = res['user'];
      },
      err => {
        console.log(err);

      }
    );
  }

  getCampaigns() {
    this.campaignService.getCampaign().subscribe(
      res => {
        this.campaigns = res;
        this.calculateTableData();
        this.calculateStatistics();
        this.dataSource.paginator = this.paginator;
      },
      err => {
        console.log(err);

      }
    );
  }

  calculateTableData() {
    this.dataSource = new MatTableDataSourceWithCustomSort(this.campaigns);
    this.dataSource.sort = this.sort;
    this.campaignsAmount = this.campaigns.length;
  }

  calculateStatistics() {
    if ((this.totalCoupons === 0) && (this.totalMiota === 0)) {
      for (const campaign of this.campaigns) {
        this.totalCoupons += campaign.amount;
        this.totalMiota += campaign.amount * campaign.value;
        this.couponsRedeemed += campaign.redeemed;
      }
    }
  }

  onLogout() {
    this.userService.deleteToken();
    this.router.navigate(['/start']);
  }

  applyFilter(filterValue: string) {
    this.dataSource.filter = filterValue.trim().toLowerCase();
  }

  openDialog(): void {
    // tslint:disable-next-line: no-use-before-declare
    const dialogRef = this.dialog.open(AddDialogComponent, {
      autoFocus: true,
      width: '260px',
    });

    dialogRef.afterClosed().subscribe(() => {
      this.getCampaigns();
    });
  }

}

export interface CampaignList {
  campaignName: string;
  amount: number;
  value: number;
  payed: boolean;
  redeemed: number;
  date: Date;
}

@Component({
  selector: 'app-dialog-add',
  templateUrl: 'app-dialog-add.html',
})
export class AddDialogComponent implements OnInit {

  addForm: FormGroup;
  submitted = false;

  constructor(private fb: FormBuilder, public dialogRef: MatDialogRef<AddDialogComponent>, private campaignService: CampaignService,
    private router: Router) {
    // To initialize FormGroup
    this.addForm = fb.group({
      'campaignName': new FormControl('', [
        Validators.required,
      ]),
      'amount': new FormControl('', [
        Validators.required,
        Validators.pattern('^[0-9]*$'),
        Validators.min(1),
        Validators.max(120),
      ]),
      'value': new FormControl('', [
        Validators.required,
        Validators.pattern('^[0-9]*$'),
        Validators.min(0),
        Validators.max(10000),
      ]),
    });
  }

  serverErrorMessages: string;

  ngOnInit() {
  }

  // convenience getter for easy access to form fields
  get f() { return this.addForm.controls; }

  async onFormSubmit() {
    const form = this.addForm.value;
    this.submitted = true;
    // stop here if form is invalid
    if (this.addForm.invalid) {
      return;
    }
    form.date = new Date();
    this.campaignService.postCampaign(form).subscribe(
      (res: any) => {
        this.dialogRef.close();
        this.router.navigateByUrl('/campaign/' + res._id);
      },
      err => {
        this.submitted = false;
        this.serverErrorMessages = err.error.message;
      }
    );
  }

  onNoClick(): void {
    this.dialogRef.close();
  }



}
