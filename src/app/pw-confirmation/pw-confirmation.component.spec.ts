import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { PwConfirmationComponent } from './pw-confirmation.component';

describe('PwConfirmationComponent', () => {
  let component: PwConfirmationComponent;
  let fixture: ComponentFixture<PwConfirmationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ PwConfirmationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(PwConfirmationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
