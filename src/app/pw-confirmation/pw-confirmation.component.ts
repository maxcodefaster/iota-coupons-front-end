import { Component, OnInit, NgZone, ViewChild, AfterViewInit } from '@angular/core';
import { ParamMap, ActivatedRoute, Router } from '@angular/router';
import { UserService } from '../shared/user.service';
import { NgForm, FormGroup, Validators, FormControl, FormBuilder } from '@angular/forms';
import { MatSnackBar } from '@angular/material/snack-bar';

// import custom validator to validate that pw and confirm pw fields match
import { MustMatch } from '../_helpers/must-match.validator';
import { RecaptchaComponent } from 'ng-recaptcha';

@Component({
  selector: 'app-pw-confirmation',
  templateUrl: './pw-confirmation.component.html',
  styleUrls: ['./pw-confirmation.component.css']
})
export class PwConfirmationComponent implements OnInit {
  @ViewChild('captchaRef', { static: true }) captchaRef: RecaptchaComponent;

  emailRegex = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  serverMessage;
  responseType;
  submitted = false;
  loggedIn = false;
  changePwForm: FormGroup;
  token;
  model = {
    email: '',
    password: '',
    recaptcha: '',
  };

  constructor(private fb: FormBuilder, private route: ActivatedRoute, private userService: UserService, private router: Router,
    private _snackBar: MatSnackBar, private zone: NgZone) {
    // To initialize FormGroup
    this.changePwForm = this.fb.group({
      pw: ['', [Validators.required, Validators.minLength(8)]],
      confirmPw: ['', [Validators.required]],
      recaptchaReactive: [null, Validators.required]
    }, {
      validator: MustMatch('pw', 'confirmPw')
    });
  }

  ngOnInit() {
    if (this.userService.isLoggedIn()) {
      this.loggedIn = true;
    }
    this.route.paramMap.subscribe((params: ParamMap) => {
      this.token = params.get('id');
    });
  }

  changePw() {
    this.captchaRef.resolved.subscribe(captchaResponse => this.changePwSubmit(captchaResponse, this.changePwForm));
    this.captchaRef.execute();
  }

  changePwSubmit(captchaResponse, form) {
    // stop here if form is invalid
    if (this.changePwForm.invalid) {
      return;
    }
    form.value.recaptcha = captchaResponse;
    form.value.token = this.token;
    this.userService.updatePw(form.value).subscribe(
      (res: any) => {
        this.serverMessage = res.message;
        this.responseType = res.type;
        this.openSnackBar(this.serverMessage);
        this.submitted = true;
        if (this.loggedIn) {
          this.userService.deleteToken();
        }
        this.captchaRef.reset();
      },
      err => {
        this.responseType = err.error.type;
        this.serverMessage = err.error.message;
        this.openSnackBar(this.serverMessage);
        this.captchaRef.reset();
      }
    );
  }

  // convenience getter for easy access to form fields
  get f() { return this.changePwForm.controls; }

  onSubmitLogin(form: NgForm) {
    this.userService.login(form.value).subscribe(
      res => {
        this.userService.setToken(res['token']);
        this.router.navigateByUrl('/dashboard');
      },
      err => {
        this.serverMessage = err.error.message;
        this.openSnackBar(this.serverMessage);
      }
    );
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'OK', {
      duration: 2000,
    });
  }

}
