import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { CampaignService } from '../shared/campaign.service';
import { delay } from 'rxjs/operators';
import { MatSnackBar } from '@angular/material/snack-bar';
import { MAT_STEPPER_GLOBAL_OPTIONS } from '@angular/cdk/stepper';
import * as IotaQR from "@tangle-frost/iota-qr-lib/pkg/iota-qr-lib.js";

@Component({
  selector: 'app-campaign',
  templateUrl: './campaign.component.html',
  styleUrls: ['./campaign.component.css'],
  providers: [{
    provide: MAT_STEPPER_GLOBAL_OPTIONS, useValue: { displayDefaultIndicatorType: false }
  }]
})
export class CampaignComponent implements OnInit {

  campaign;
  copyText = 'Click to copy';
  coupons;
  pdfSrc;
  borders;
  logos;
  pdfLoader = true;
  backgroundImage;
  borderImage;
  couponDescription;
  couponDescription1;
  couponDescription2;
  couponDescription3;
  colorBorder = '#000';
  colorLogo = '#ccc';
  colorText = '#000';
  qrCodeData = null

  constructor(private route: ActivatedRoute, private campaignService: CampaignService, private _snackBar: MatSnackBar) { }

  ngOnInit() {
    let campaignId = '';
    this.route.paramMap.subscribe((params: ParamMap) => {
      campaignId = params.get('id');
    });
    this.getCampaign(campaignId);
    this.getResources();
  }

  getCampaign(id) {
    this.campaignService.getSpecificCampaign(id).subscribe(
      res => {
        this.campaign = res;
        if (!this.campaign.payed) {
          const paymentData = IotaQR.TrinityPaymentQR.generatePaymentData(
            this.campaign.paymentAddress,
            this.campaign.value * this.campaign.amount * 1000000,
            "",
            ""
          );
          IotaQR.TrinityPaymentQR.renderHtml(paymentData, "png", 8).then(
            qrCodeData => {
              this.qrCodeData = qrCodeData;
              this.getCampaign(id)
            }
          );

        }
        this.getCoupons(id);
      },
      err => {
        console.log(err);
      }
    );
  }

  getCoupons(id) {
    this.campaignService.getCoupons(id).subscribe(
      res => {
        this.coupons = res;
      },
      err => {
        console.log(err);
      }
    );
  }

  getResources() {
    this.campaignService.getBorders().subscribe(
      (res: any) => {
        this.borders = res;
        this.borderImage = this.borders[0];
      },
      err => {
        console.log(err);
      }
    );
    this.campaignService.getLogos().subscribe(
      (res: any) => {
        this.logos = res;
        this.backgroundImage = this.logos[0];
      },
      err => {
        console.log(err);
      }
    );
  }

  onStepChange($event) {
    this.pdfLoader = true;
    if ($event.selectedIndex === 2 && this.coupons) {
      this.getPdf(this.coupons);
    }
  }

  transformText($event) {
    this.couponDescription1 = $event.slice(0, 20);
    this.couponDescription2 = $event.slice(20, 40);
    this.couponDescription3 = $event.slice(40, 60);
  }

  getPdf(coupons) {
    const body = {
      coupons: coupons,
      text1: this.couponDescription1,
      text2: this.couponDescription2,
      text3: this.couponDescription3,
      colorText: this.colorText,
      colorBorder: this.colorBorder,
      colorLogo: this.colorLogo,
      logoPath: this.backgroundImage.path,
      borderPath: this.borderImage.path,
    };
    this.campaignService.createPdf(body).subscribe(
      (res: any) => {
        this.pdfSrc = res.data;
        this.pdfLoader = false;
      },
      err => {
        console.log(err);
      }
    );
  }

  copyAddress() {
    this.copyText = 'Successfully copied to clipboard';
    const val = this.campaign.paymentAddress;
    const selBox = document.createElement('textarea');
    selBox.style.position = 'fixed';
    selBox.style.left = '0';
    selBox.style.top = '0';
    selBox.style.opacity = '0';
    selBox.value = val;
    document.body.appendChild(selBox);
    selBox.focus();
    selBox.select();
    document.execCommand('copy');
    document.body.removeChild(selBox);
  }

  changeTextColor($event) {
    this.colorText = $event;
  }

  changeBorderColor($event) {
    this.colorBorder = $event;
  }

  changeLogoColor($event) {
    this.colorLogo = $event;
  }

  designChange() {
    this.openSnackBar('Applied coupon design');
  }

  openSnackBar(message: string) {
    this._snackBar.open(message, 'OK', {
      duration: 1000,
    });
  }

}
